FROM gitlab-registry.cern.ch/linuxsupport/alma9-base:latest

LABEL maintainer="OpenShift Admins <openshift-admins@cern.ch>"

# RUNTIME_DIRECTORY needs to be writable by arbitrary user IDs.
# See recommendations in https://docs.okd.io/4.10/openshift_images/create-images.html#images-create-guide-openshift_create-images
ENV RUNTIME_DIRECTORY=/run/kerberos-ticket-manager

COPY scripts/* /

RUN yum install epel-release -y && \
    yum install -y kstart && yum clean all && \
    mkdir -p "${RUNTIME_DIRECTORY}" && \
    chgrp -R 0 "${RUNTIME_DIRECTORY}" && chmod -R g=u "${RUNTIME_DIRECTORY}" && \
    chmod +x /run.sh /check.sh

ENTRYPOINT ["/run.sh"]

# by default, run in "daemon" mode (renew indefinitely)
CMD [ "daemon" ]
