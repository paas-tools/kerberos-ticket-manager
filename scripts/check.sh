#!/bin/sh

# Verify that we have a valid kerberos ticket
# NB: KRB5CCNAME env var determines where Kerberos credentials cache will be written
exec klist -s
