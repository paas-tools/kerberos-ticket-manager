#!/bin/sh

# This script takes care of obtaining a Kerberos ticket from username/password and storing it in a credentials cache file.
# The credentials cache file will be created at path indicated by env var KRB5CCNAME (or default /tmp/krb5cc_<uid>).
# With the "daemon" argument we run in daemon mode (k5start -K) and renew the ticket indefinitely.
# With the "once" argument we just generate the credentials cache file and exit.
# We expect to find username and password in the KEYTAB_USER and KEYTAB_PWD env vars.
# KRB5CCNAME env var determines where Kerberos credentials cache will be written.

set -e

mode=$1

case "${mode}" in

  "once")
    k5start_daemon_args=""
    ;;

  "daemon")
    # see man page https://linux.die.net/man/1/k5start
    k5start_daemon_args="-K 30"
    ;;

  *)
    echo "Invalid mode: $mode. Supported modes are 'once' and 'daemon'" 1>&2
    exit 1
    ;;
esac

# RUNTIME_DIRECTORY defined in Dockerfile
KEYTAB_PATH=${RUNTIME_DIRECTORY}/keytab

# Generate a keytab from them (keytab is required for k5start to keep Kerberos ticket alive indefinitely
# https://linux.die.net/man/1/ktutil
ktutil <<EOF
addent -password -p ${KEYTAB_USER}@CERN.CH -k 1 -f
${KEYTAB_PWD}
wkt ${KEYTAB_PATH}
quit
EOF

# k5start takes care of obtaining a Kerberos ticket from username/password and renewing the ticket as necessary.
# We use `-x` option so k5start exits on any error - this is mostly useful in case ticket cannot be renewed
# because the password changed: this will cause the container to restart and re-run k5start with fresh username/password.
exec k5start ${k5start_daemon_args} -l 24h -x -f "${KEYTAB_PATH}" -U
