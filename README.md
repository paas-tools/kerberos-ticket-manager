# kerberos-ticket-manager

A helper image to maintain a Kerberos ticket from credentials. Primarily used for EOS support in OKD4. 

Use argument `once` to just create the credentials cache and exit immediately.

Use argument `daemon` to run in daemon mode, renewing the credentials cache indefinitely.
